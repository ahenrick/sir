#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 11:00:35 2020

@author: exigenthesed
This code solves the basic SIR model given 
   eps = initial fraction of pop infected
   val = initial fraction of pop vaccinated
   Ro = ratio of charactersitic recovery time to 
                 charactersitic infection time
                 
Postprocesses the fraction of pop that would have a 
postive PCR test if the virus is detected even after 
recovery for dtpcr charactersitic recovery times.
   
Finally, assumes a death rate to compute number of
fatalities with the virus present.
"""

eps,Ro=.01,2 # intial % of pop infected, Ro
vac=.0 # % of pop vaccinated
dtpcr=3 # dimless PCR detection time post recovery

Npcr=100 # # of sampling intervals per PCR detection time
N=5*Npcr # total # of time intervals integration

import numpy as np
from scipy.integrate import odeint, cumtrapz
import matplotlib.pyplot as plt

def sir(x,y,r,Ro):
    """
    SIR model

    Parameters
    ----------
    x : float: % suceptible
    y : float: % infected
    r : float: % recovered
    Ro : float: characteristic recovery time/char infection time

    Returns
    -------
    numerical array of floats: [x,y,r]'

    """
    dxdt=-Ro*x*y
    dydt=Ro*x*y-y
    drdt=y
    return np.array([dxdt,dydt,drdt])

dt=dtpcr/Npcr # dimless sampling interval
tf=N*dt # dimless total time of integration
t=np.linspace(0,tf,N+1) # dimless sampling times
Yo=np.array([1-eps-vac,eps,vac]) # initial condition

plt.close('all')
fig=plt.figure()
ax1=fig.add_subplot(211)
ax2=fig.add_subplot(212
                    )
# plot 'flattening the curve'
hit=[]
RRo=np.linspace(1,5,5)
for R in RRo:
    Y=odeint(lambda Y,t: sir(*Y,R),Yo,t) # solve
    x,y,r=Y[:,0],Y[:,1],Y[:,2] # % suceptible,infected,recovered
    hit.append(x[-1]*100)
    ax1.plot(t,y*100,label=r'$R_o=$'+'{0:1.1f}'.format(R))
    ax2.plot(t,x*100,label=r'$R_o=$'+'{0:1.1f}'.format(R))

ax2.set_xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
ax1.set_ylabel(r'% pop. infected')    
ax1.set_title(r'$\epsilon$='+'{0:1.2f}'.format(eps))
ax2.set_ylabel(r'% pop. susceptible')   
ax1.legend(); ax2.legend()
ax1.grid(True); ax2.grid(True)
fig.tight_layout()

fig=plt.figure()
plt.plot(RRo,hit,'--')
plt.grid(True)
plt.xlabel(r'$R_o$')
plt.ylabel('Final % Population Never Infected')

fig=plt.figure()
Y=odeint(lambda Y,t: sir(*Y,Ro),Yo,t) # solve
x,y,r=Y[:,0],Y[:,1],Y[:,2] # % suceptible,infected,recovered
# % population positive pcr test
# p=y(t)+r(t)-r(t-dtpcr)
p=np.empty(np.size(y)) 
p[0:Npcr]=y[0:Npcr]+r[0:Npcr]-vac # t < dtpcr
p[Npcr:]=y[Npcr:]+r[Npcr:]-r[0:N-Npcr+1] # t >= dtpcr

plt.plot(t,Y)
plt.plot(t,p,'--')
plt.legend(('susceptible','infected','recovered','positive pcr'))
plt.xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
plt.title(f"{Ro=:G},{eps=:G},{dtpcr=:G},{vac=:G}")
plt.grid('True')
plt.show()
