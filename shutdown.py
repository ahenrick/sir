#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

eps,Ro,Rsh=.01,3,1.5    # initial % of pop infected, Ro's
N=500           # # of time intervals per integration
Np1=N+1         # # of points per integration
Nm1=N-1         # index of 
tf,dtsh=10,1    # final integration & shutdown time
i1=slice(0,Np1)
i2=slice(N,2*N+1)
i3=slice(2*N,3*N+1)
def sir(x,y,r,Ro):
    """
    SIR model

    Parameters
    ----------
    x : float: % suceptible
    y : float: % infected
    r : float: % recovered
    Ro : float: characteristic recovery time/char infection time

    Returns
    -------
    numerical array of floats: [x,y,r]'

    """
    dxdt=-Ro*x*y
    dydt=Ro*x*y-y
    drdt=y
    return np.array([dxdt,dydt,drdt])


ttsh=np.linspace(.1,3,1000)
ymax=np.empty_like(ttsh)
t=np.empty(3*N+1)
Y=np.empty((3*N+1,3))

for i in range(len(ttsh)):
    tsh = ttsh[i]
    dt=tsh/N                        # dimless sampling interval
    t[i1]=np.linspace(0,tsh,Np1)    # dimless sampling times
    t[i2]=np.linspace(tsh,tsh+dtsh,Np1) 
    t[i3]=np.linspace(tsh+dtsh,tf,Np1) 
    Yo=np.array([1-eps,eps,0])      # initial condition
    Y[i1,:]=odeint(lambda Y,t: sir(*Y,Ro),Yo,t[i1]) # solve  
    Yo=Y[N,:]                       # initial condition
    Y[i2,:]=odeint(lambda Y,t: sir(*Y,Rsh),Yo,t[i2]) # solve 
    Yo=Y[2*N,:]                     # initial condition
    Y[i3,:]=odeint(lambda Y,t: sir(*Y,Ro),Yo,t[i3]) # solve
    ymax[i]=np.max(Y[:,1])          # max % infected

plt.plot(ttsh,ymax)
plt.xlabel(r'$t^* = \frac{t_{shutdown}}{t_{recovery}}$')
plt.ylabel('Maximum % infected')
plt.title(f'Shutdown effectiveness: {Ro=:G},{Rsh=:G},{eps=:G},{dtsh=:G}')
plt.grid()
plt.show()

i=np.argmin(ymax)
tsh = ttsh[i]
dt=tsh/N # dimless sampling interval
t[i1]=np.linspace(0,tsh,Np1) # dimless sampling times
t[i2]=np.linspace(tsh,tsh+dtsh,Np1) # dimless sampling times
t[i3]=np.linspace(tsh+dtsh,tf,Np1)
Yo=np.array([1-eps,eps,0]) # initial condition
Ystd=odeint(lambda Y,t: sir(*Y,Ro),Yo,t)
Y[i1,:]=odeint(lambda Y,t: sir(*Y,Ro),Yo,t[i1]) # solve
Yo=Y[N,:] # initial condition
Y[i2,:]=odeint(lambda Y,t: sir(*Y,Rsh),Yo,t[i2]) # solve
Yo=Y[2*N,:] # initial condition
Y[i3,:]=odeint(lambda Y,t: sir(*Y,Ro),Yo,t[i3]) # solve
x,y,r=Y[:,0],Y[:,1],Y[:,2] # % suceptible,infected,recovered

plt.figure()
plt.plot(t,Y)
plt.plot(t,Ystd,'-.')
plt.legend(('susceptible','infected','recovered'))
plt.xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
plt.title(f"{Ro=:G},{Rsh=:G},{eps=:G},{tsh=:G},{dtsh=:G}")
plt.grid('True')
plt.show()
