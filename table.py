def table(**data_dict):
    n=([len(x) for x in data_dict.values()]) # # data for each var
    N=max(n)
    # prep data & print headings
    prntstr=''
    for i,var in enumerate(data_dict):
        # convert lists to str for printing, padding as needed
        data_dict[var] = ['{:.5g}'.format(x) 
                          for x in data_dict[var]] + ['']*(N-n[i]) 
        prntstr=prntstr+f'| {var:^8} |'
    print(prntstr,'-'*12*len(data_dict),sep='\n'); # print heading 
    # print data
    for i in range(0,N):
        prntstr=' '.join(f'{data_dict[var][i]:>11}' for var in data_dict)
        print(prntstr)
