#!/usr/bin/env python3
# -*- coding: utf-8 -*-
eps,RRo=.01,(2,3) # intial % of pop infected, Ro
trec=14 # days recovery time
# estimate 8.4 deaths/1k persons annually
mu=(8.4/1000)*trec/365 # dimless death rate
# trec*dtpcr days of PCR detection after recovery
dtpcr=3 # dimless PCR detection time post recovery

Npcr=100 # # of sampling intervals per PCR detection time
N=5*Npcr # total # of time intervals integration

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

def sir(x,y,r,yf,rf,Ro):
    """
    SIR model

    Parameters
    ----------
    x : float: % suceptible
    y : float: % infected
    r : float: % recovered
    yf : float: % infectious fatalities
    rf : float: % recovered fatalities
    Ro : float: characteristic recovery time/char infection time

    Returns
    -------
    numerical array of floats: [x,y,r]'

    """
    dxdt=-Ro*x*y-mu*x
    dydt=Ro*x*y-y-mu*y
    drdt=y-mu*r
    dyfdt=mu*y
    drfdt=mu*r
    return np.array([dxdt,dydt,drdt,dyfdt,drfdt])

dt=dtpcr/Npcr # dimless sampling interval
tf=N*dt # dimless total time of integration
t=np.linspace(0,tf,N+1) # dimless sampling times
cmap = plt.get_cmap('tab10')
Yo=np.array([1-eps,eps,0,0,0]) # initial condition
for i,Ro in enumerate(RRo):
    Y=odeint(lambda Y,t: sir(*Y,Ro),Yo,t) # solve
    x,y,r,yf,rf=Y[:,0],Y[:,1],Y[:,2],Y[:,3],Y[:,4] # % s, i, r, if, rf
    
    # dpdt=mu*y+mu*r-mu*r(t-dtpcr) & dndt=mu*x+mu*r(t-dtpcr)
    # p=yf(t)+rf(t)-rf(t-dtpcr) cumulative positive pcr tests to data
    p=np.empty_like(yf) 
    p[0:Npcr]=yf[0:Npcr]+rf[0:Npcr] # t < dtpcr
    p[Npcr:]=yf[Npcr:]+rf[Npcr:]-rf[0:N-Npcr+1] # t >= dtpcr
    plt.figure(0)
    plt.plot(t*trec,yf*1e5,label=f'infected: $R_o=${Ro}',color=cmap(i))
    plt.plot(t*trec,p*1e5,'--',label=f'positive pcr: $R_o=${Ro}'
             ,color=cmap(i))
    dpdt=np.empty_like(y)
    dpdt[0:Npcr]=mu*y[0:Npcr]+mu*r[0:Npcr]
    dpdt[Npcr:]=mu*y[Npcr:]+mu*r[Npcr:]-mu*r[0:N-Npcr+1]
    plt.figure(1)
    plt.plot(t*trec,mu*y*1e5/trec,label=f'infected: $R_o=${Ro}'
             ,color=cmap(i))
    plt.plot(t*trec,dpdt*1e5/trec,'--',label=f'positive pcr: $R_o=${Ro}'
             ,color=cmap(i))

plt.figure(0)
plt.xlabel(r'$t$ [days]')
plt.ylabel(r'deaths/100k')
plt.title(f'Natural Deaths: $\mu=${mu:.1e},$\epsilon=${eps:G},'+
          f'$\Delta t^*_{{pcr}}=${dtpcr:G},$t_{{rec}}=${trec:G} days')
plt.grid('True')
plt.legend()
#plt.xlim(0,375); plt.ylim(.3467,188.52)
#plt.show()

plt.figure(1)
plt.xlabel('t [days]')
plt.ylabel(r'$\frac{deaths/100k}{day}$')
plt.title(f'Natural Deaths: $\mu=${mu:.1e},$\epsilon=${eps:G},'+
          f'$\Delta t^*_{{pcr}}=${dtpcr:G},$t_{{rec}}=${trec:G} days')
plt.grid('True')
plt.legend()
#plt.xlim(0,375); plt.ylim(0,2)
plt.show()

plt.figure()
plt.plot(t,y,label='SIR')
plt.plot(t[0:125],y[0]*np.exp(t[0:125]-t[0]),'--',label='exponential')
plt.xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
plt.ylabel(r'% infected')
plt.grid('True')
plt.legend()
plt.title(f'Harmless virus: $R_o=${Ro:G},$\epsilon=${eps:G}')
plt.show()

plt.figure()
plt.semilogy(t,y,label='SIR')
plt.semilogy(t[0:125],y[0]*np.exp(t[0:125]-t[0]),'--',label='exponential')
plt.xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
plt.ylabel(r'% infected')
plt.grid('True')
plt.legend()
plt.title(f'Harmless virus: $R_o=${Ro:G},$\epsilon=${eps:G}')
plt.show()

dydt=np.array([sir(*Y[i],Ro)[1] for i in range(N+1)])
plt.figure()
plt.loglog(y,dydt,'r',label='SIR - growth')
plt.loglog(y,-dydt,'b',label='SIR - decay')
plt.loglog(y,y,'--',label='exponential',color='orange')
plt.xlabel(r'$y^* =$ % infected')
plt.ylabel(r'$\left|\frac{dy^*}{dt^*}\right| = $ rate of change')
plt.grid('True')
plt.legend()
plt.title(f'Harmless virus: $R_o=${Ro:G},$\epsilon=${eps:G}')
plt.show()

plt.figure()
plt.plot(t,x*Ro)
plt.xlabel(r'$t^* = \frac{t}{t_{recovery}}$')
plt.ylabel(r'$R_e(t)$')
plt.grid('True')
plt.title(f'Harmless virus: $R_o=${Ro:G},$\epsilon=${eps:G}')
plt.show()
